#! /bin/bash

# access credentials and connection setup mostly in p3.cnf
# before using the cloned database, make sure you grant privileges for the user that is set in moodle config
DBSOURCE="moodledb"
DBDEST="moodleNoUI"


# Diff the two databases to find what has changed

mysqldump --defaults-file=config/p3.cnf --defaults-group-suffix=source --compact --skip-extended-insert --complete-insert "$DBSOURCE" > data/sourcecompare.sql
mysqldump --defaults-file=config/p3.cnf --defaults-group-suffix=dest --compact --skip-extended-insert --complete-insert "$DBDEST" > data/destcompare.sql

diff -u data/sourcecompare.sql data/destcompare.sql > data/sqldiff.patch

