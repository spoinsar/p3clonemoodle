<?php

class dbdest
{
    protected $host;
    protected $pwd;
    protected $login;
    protected $db;
    protected $dbConnect;

    /**
     * connect to database
     */
    public function __construct($info_db, $db) {
        $this->host = $info_db['host'];
        $this->db = $db;
        $this->login = $info_db['user'];
        $this->pwd = $info_db['password'];
        try {
            $this->dbConnect = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->db . ';charset=utf8', $this->login, $this->pwd);
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }
    /*
    * get all user
    */
    public function get_users(){
        $query = 'SELECT `id`, `email`, `username` FROM `mdl_user`';
        $get_user = $this->dbConnect->query($query);
        return $get_user->fetchAll(PDO::FETCH_OBJ);
    }
    /*
    * anonymise all user(username, email and name)
    */
    public function anonymous_user($user, $hash_key){
        $hash_dict = array();
        $hash = hash_hmac('sha256', $user->email, $hash_key);
        $hash_dict['mail'] = $hash . '@mailbidon.fr';
        $hash_dict['username'] = substr($hash, 0, 10) . $user->id;
        $hash_dict['firstname'] = substr($hash_dict['username'], 0, 5);
        $hash_dict['lastname'] = substr($hash_dict['username'], 5, 5);
        return $this->update_user($user->id, $hash_dict);
    }
    /*
    * update user(username, email and name)
    */
    public function update_user($id, $user_a){
        $query = 'UPDATE `mdl_user` SET email=:email, username=:username, firstname=:firstname, lastname=:lastname WHERE id=:id';
        $update_user = $this->dbConnect->prepare($query);
        $update_user->bindValue(':email', $user_a['mail'], PDO::PARAM_STR);
        $update_user->bindValue(':username', $user_a['username'], PDO::PARAM_STR);
        $update_user->bindValue(':firstname', $user_a['firstname'], PDO::PARAM_STR);
        $update_user->bindValue(':lastname', $user_a['lastname'], PDO::PARAM_STR);
        $update_user->bindValue(':id', $id, PDO::PARAM_INT);
        return $update_user->execute();
    }
}