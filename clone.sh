#! /bin/bash

# This script will clone a moodle database, and apply changes to make it ready to process for learning analytics
# (enable webservices, configure logstore_xapi)
#
# You must already have the 2 moodle instances on a webserver.
# The new copied database must be loaded in a version for this server



# check every file in the config/ directory
# some settings must be changed

# will set most variable in this script (DB*, CLONE*, PWDEST...)
source config/config.sh

> start_end
echo "start : " >> start_end
echo $(date +"%s") >> start_end
file_name_log="first_log"
	if test -f $file_name_log; then
		first_log=$(mysql --defaults-file=config/p3.cnf --defaults-group-suffix=dest -e "SELECT id FROM $DBDEST.mdl_logstore_xapi_log ORDER BY id ASC LIMIT 1;")
		WORDTOREMOVE="id"
		first_log=${first_log//$WORDTOREMOVE/}
		if [[ $first_log == "" ]]; then
			first_log=$(mysql --defaults-file=config/p3.cnf --defaults-group-suffix=dest -e "SELECT id FROM $DBDEST.mdl_logstore_standard_log ORDER BY id DESC LIMIT 1;")
			first_log=${first_log//$WORDTOREMOVE/}
			first_log=$(($first_log + 1))
		fi
    rm $file_name_log
    echo $first_log >> $file_name_log
else
    echo 1 >> $file_name_log
    first_log=1
fi

echo "backup moodle-j-1"
# backup the j-1 database (already done outside, for p3 ?)
mysqldump --defaults-file=config/p3.cnf --defaults-group-suffix=source --max_allowed_packet=512M "$DBSOURCE" > data/p3jm1.sql

echo "restore moodle-j-1 in moodletmp"
# restore the backup on moodle tempory
mysql --defaults-file=config/p3.cnf --defaults-group-suffix=tmp -e "DROP DATABASE IF EXISTS $DBTMP; CREATE DATABASE $DBTMP;"
echo "backup moodletmp"
mysql --defaults-file=config/p3.cnf --defaults-group-suffix=tmp -s "$DBTMP" < data/p3jm1.sql
#rm data/p3jm1.sql

echo "anonymize user"
# anonymize user data !
erroranon=$(php anonymize.php $DBTMP)
if [[ $erroranon != 1 ]]; 
then
    echo $erroranon
    exit
fi


# check if we have correctly enabled the "noemailever" mode for the cloned Moodle
# we would not want to spam our teachers and students from a clone they dont care about
if ! php --run "define(\"CLI_SCRIPT\", true); include \"$CLONECODE/config.php\"; if (\$CFG->noemailever) { exit(0);} else {exit(1);} ;" ; then
	echo "<html><body>Passage en sécurité : pour faire tourner cette version de moodle learning analytics, il faut désactiver l'envoie de mails (\$CFG->noemailever dans config.php). Faites la modification et réessayez.</body></html>" > "$CLONEDATA/climaintenance.html"
	echo "Mail output must be disabled to run this script. Moodle clone maintenance mode activated until you do so." >&2
	exit 1
fi

# backup tempory database
mysqldump --defaults-file=config/p3.cnf --defaults-group-suffix=tmp --max_allowed_packet=512M "$DBTMP" > data/p3clonetmp.sql
#mysql --defaults-file=config/p3.cnf --defaults-group-suffix=tmp -e "DROP DATABASE IF EXISTS $DBTMP;"

echo "restore moodletmp in moodlecopie"
# restore the backup on moodle clone
echo "<html><body>Actualisation de la base de donnée en cours, accès temporairement indisponible</body></html>" > "$CLONEDATA/climaintenance.html"
mysql --defaults-file=config/p3.cnf --defaults-group-suffix=dest -e "DROP DATABASE IF EXISTS $DBDEST; CREATE DATABASE $DBDEST;"
mysql --defaults-file=config/p3.cnf --defaults-group-suffix=dest -s "$DBDEST" < data/p3clonetmp.sql
#rm data/p3clonetmp.sql

echo "conf final"
# enable the logstore_xapi plugin silently
sudo -u www-data /usr/bin/php "$CLONECODE/admin/cli/upgrade.php" --non-interactive

# add webservice support and configure the logstore_xapi plugin
mysql --defaults-file=config/p3.cnf --defaults-group-suffix=tmp -s "$DBDEST" < clonepatch.sql

# after changes in database, need to purge all caches
sudo -u www-data /usr/bin/php "$CLONECODE/admin/cli/purge_caches.php"

# copy log moodle in xapi log
mysql --defaults-file=config/p3.cnf --defaults-group-suffix=tmp -e "DELETE FROM $DBDEST.mdl_logstore_xapi_log; INSERT INTO $DBDEST.mdl_logstore_xapi_log SELECT * FROM $DBDEST.mdl_logstore_standard_log WHERE id >= $first_log;"

# reset admin password
sudo -u www-data /usr/bin/php "$CLONECODE/admin/cli/reset_password.php" --username=admin "--password=$PWDEST" --ignore-password-policy

rm -f "$CLONEDATA/climaintenance.html"

echo " end : " >> start_end
echo $(date +"%s") >> start_end
