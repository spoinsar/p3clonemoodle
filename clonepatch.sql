-- this file should be applyed after cloning the database



-- -------------------------------------------------------
-- Enable Webservices
-- -------------------------------------------------------


-- #TODO : get the token from an external file ?
-- something like "source conf/wstoken.sql" with the next line would work ?
source config/wstoken.sql;
source config/lrs.sql;

-- delete all private message
DELETE FROM `mdl_messages`;
DELETE FROM `mdl_message_read`;
DELETE FROM `mdl_message_user_actions`;
DELETE FROM `mdl_message_conversation_members`;
DELETE FROM `mdl_message_conversations`;

-- enable webservices
UPDATE `mdl_config` SET value=1 WHERE name='enablewebservices';

-- webserviceprotocols setting does not exist if webservice not enabled on the original moodle, so make sure we do an insert and not just an update
DELETE FROM `mdl_config` WHERE name='webserviceprotocols';
INSERT INTO `mdl_config` (`name`, `value`) VALUES ('webserviceprotocols','rest');


-- create role for webservice user
INSERT INTO `mdl_role` (`name`, `shortname`, `description`, `sortorder`, `archetype`) VALUES ('webservice','webservice','',9999999,'manager');
SET @roleid = LAST_INSERT_ID();

-- duplicate the admin role permissions(roleid = 1), and apply it to the webservice role
-- we copy the admin role in a temporary table, set it as our own webservice role insert id, and copy it back in the role table
CREATE TEMPORARY TABLE tmptable SELECT * FROM mdl_role_allow_assign WHERE roleid = 1;
UPDATE tmptable SET roleid=@roleid;
INSERT INTO mdl_role_allow_assign (`roleid`, `allowassign`) SELECT `roleid`, `allowassign` FROM tmptable;
DROP TEMPORARY TABLE IF EXISTS tmptable;

CREATE TEMPORARY TABLE tmptable SELECT * FROM mdl_role_allow_view WHERE roleid = 1;
UPDATE tmptable SET roleid=@roleid;
INSERT INTO mdl_role_allow_view (`roleid`, `allowview`) SELECT `roleid`, `allowview` FROM tmptable;
DROP TEMPORARY TABLE IF EXISTS tmptable;

CREATE TEMPORARY TABLE tmptable SELECT * FROM mdl_role_allow_override WHERE roleid = 1;
UPDATE tmptable SET roleid=@roleid;
INSERT INTO mdl_role_allow_override (`roleid`, `allowoverride`) SELECT `roleid`, `allowoverride` FROM tmptable;
DROP TEMPORARY TABLE IF EXISTS tmptable;

CREATE TEMPORARY TABLE tmptable SELECT * FROM mdl_role_allow_switch WHERE roleid = 1;
UPDATE tmptable SET roleid=@roleid;
INSERT INTO mdl_role_allow_switch (`roleid`, `allowswitch`) SELECT `roleid`, `allowswitch` FROM tmptable;
DROP TEMPORARY TABLE IF EXISTS tmptable;

-- role_capability is almost the same but different table structure
CREATE TEMPORARY TABLE tmptable SELECT * FROM mdl_role_capabilities WHERE roleid = 1;
UPDATE tmptable SET roleid=@roleid;
INSERT INTO mdl_role_capabilities (`contextid`, `roleid`, `capability`, `permission`) SELECT `contextid`, `roleid`, `capability`, `permission` FROM tmptable;
DROP TEMPORARY TABLE IF EXISTS tmptable;

-- role_context_levels also different structure
CREATE TEMPORARY TABLE tmptable SELECT * FROM mdl_role_context_levels WHERE roleid = 1;
UPDATE tmptable SET roleid=@roleid;
INSERT INTO mdl_role_context_levels (`roleid`, `contextlevel`) SELECT `roleid`, `contextlevel` FROM tmptable;
DROP TEMPORARY TABLE IF EXISTS tmptable;

-- admin may not have the REST role, so we add it to the webservice role
DELETE FROM `mdl_role_capabilities` WHERE roleid=@roleid AND capability='webservice/rest:use';
INSERT INTO `mdl_role_capabilities` (`contextid`, `roleid`, `capability`, `permission`) VALUES (1,@roleid,'webservice/rest:use',1);


-- create the webservice user
INSERT INTO `mdl_user` (`auth`, `confirmed`, `policyagreed`, `deleted`, `suspended`, `mnethostid`, `username`, `password`, `idnumber`, `firstname`, `lastname`, `email`, `emailstop`, `icq`, `skype`, `yahoo`, `aim`, `msn`, `phone1`, `phone2`, `institution`, `department`, `address`, `city`, `country`, `lang`, `calendartype`, `theme`, `timezone`, `firstaccess`, `lastaccess`, `lastlogin`, `currentlogin`, `lastip`, `secret`, `picture`, `url`, `description`, `descriptionformat`, `mailformat`, `maildigest`, `maildisplay`, `autosubscribe`, `trackforums`, `timecreated`, `timemodified`, `trustbitmask`) VALUES ('manual',1,0,0,0,1,'ws','$2y$10$nVR2Aaj2f.cRh/wF.UJs/ucn33XOSofIyaAkjHiIfu1Cd5IQTu7gu','','Webservice P3','-','noreply@example.org',0,'','','','','','','','','','','','','en','gregorian','','99',0,0,0,0,'','',0,'','',1,1,0,2,1,0,1607323876,1607324351,0);
SET @userid = LAST_INSERT_ID();

-- token used for webservices
INSERT INTO `mdl_user_private_key` (`script`, `value`, `userid`, `instance`, `iprestriction`, `validuntil`, `timecreated`) VALUES ('core_files',@userkey,@userid,NULL,NULL,NULL,1607324401);

-- definition of the webservice that will have all the functions used in our LA scripts
INSERT INTO `mdl_external_services` (`name`, `enabled`, `requiredcapability`, `restrictedusers`, `component`, `timecreated`, `timemodified`, `shortname`, `downloadfiles`, `uploadfiles`) VALUES ('p3ws',1,'',1,NULL,1607324187,NULL,'p3ws',0,0);
SET @externalserviceid = LAST_INSERT_ID();

-- allow functions to the webservice
-- #TODO : add new lines here as needed, to declare new functions
INSERT INTO `mdl_external_services_functions` (`externalserviceid`, `functionname`) VALUES (@externalserviceid, 'core_webservice_get_site_info');
INSERT INTO `mdl_external_services_functions` (`externalserviceid`, `functionname`) VALUES (@externalserviceid, 'core_enrol_get_users_courses');
INSERT INTO `mdl_external_services_functions` (`externalserviceid`, `functionname`) VALUES (@externalserviceid, 'core_enrol_get_enrolled_users');
INSERT INTO `mdl_external_services_functions` (`externalserviceid`, `functionname`) VALUES (@externalserviceid, 'core_course_get_courses_by_field');
INSERT INTO `mdl_external_services_functions` (`externalserviceid`, `functionname`) VALUES (@externalserviceid, 'core_course_get_contents');
INSERT INTO `mdl_external_services_functions` (`externalserviceid`, `functionname`) VALUES (@externalserviceid, 'mod_quiz_get_quizzes_by_courses');
INSERT INTO `mdl_external_services_functions` (`externalserviceid`, `functionname`) VALUES (@externalserviceid, 'mod_assign_get_submissions');
INSERT INTO `mdl_external_services_functions` (`externalserviceid`, `functionname`) VALUES (@externalserviceid, 'mod_assign_get_assignments');
INSERT INTO `mdl_external_services_functions` (`externalserviceid`, `functionname`) VALUES (@externalserviceid, 'mod_assign_list_participants');
INSERT INTO `mdl_external_services_functions` (`externalserviceid`, `functionname`) VALUES (@externalserviceid, 'mod_assign_get_grades');



-- grant ws usage to the webservice user
INSERT INTO `mdl_external_services_users` (`externalserviceid`, `userid`, `iprestriction`, `validuntil`, `timecreated`) VALUES (@externalserviceid,@userid,NULL,NULL,1607324221);

INSERT INTO `mdl_external_tokens` (`token`, `privatetoken`, `tokentype`, `userid`, `externalserviceid`, `sid`, `contextid`, `creatorid`, `iprestriction`, `validuntil`, `timecreated`, `lastaccess`) VALUES (@token,@privtoken,0,@userid,@externalserviceid,NULL,1,2,NULL,0,1607324254,1607324401);

INSERT INTO `mdl_role_assignments` (`roleid`, `contextid`, `userid`, `timemodified`, `modifierid`, `component`, `itemid`, `sortorder`) VALUES (@roleid,1,@userid,1607324393,2,'',0,0);

-- -------------------------------------------------------
-- Configure fake smtp
-- -------------------------------------------------------
UPDATE `mdl_config` SET value='smtp.bidon.fr' WHERE name='smtphosts';
UPDATE `mdl_config` SET value='noreply@smtpbidon.fr' WHERE name='noreplyaddress';
-- -------------------------------------------------------
-- Configure logstore_xapi plugin
-- -------------------------------------------------------

-- setup for logstore_xapi plugin
-- #TODO : change those values depending on the xapi LRS hosting
UPDATE `mdl_config_plugins` SET value=@lrsendpoint WHERE plugin='logstore_xapi' AND name='endpoint';
UPDATE `mdl_config_plugins` SET value=@lrskey WHERE plugin='logstore_xapi' AND name='username';
UPDATE `mdl_config_plugins` SET value=@lrssecret WHERE plugin='logstore_xapi' AND name='password';

-- other config that can stay as it is
UPDATE `mdl_config_plugins` SET value='1' WHERE plugin='logstore_xapi' AND name='resendfailedbatches';
UPDATE `mdl_config_plugins` SET value='1' WHERE plugin='logstore_xapi' AND name='mbox';
UPDATE `mdl_config_plugins` SET value='1' WHERE plugin='logstore_xapi' AND name='sendresponsechoices';

-- Most recent logs should be transfered for them to be parsed
-- see https://github.com/xAPI-vle/moodle-logstore_xapi/blob/master/docs/historical-events.md

-- Copy everything from less than 1 day ago, accounting for 10 hours of backup delay and scripting time
--INSERT INTO mdl_logstore_xapi_log SELECT * FROM mdl_logstore_standard_log WHERE FROM_UNIXTIME(timecreated) >= NOW() - INTERVAL 34 HOUR;

-- Enable the log store (is it necessary if we run the task from cron ? I dont know)
UPDATE `mdl_config_plugins` SET value='logstore_standard,logstore_xapi' WHERE plugin='tool_log' AND name='enabled_stores';

