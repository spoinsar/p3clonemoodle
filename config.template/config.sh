# DIRCLONE / DIRDATA without traling slash
# must be writable
CLONECODE="/var/www/moodlep3clone"
CLONEDATA="/var/moodlep3clone"

# Access credentials and connection setup mostly in p3.cnf
# Need DROP and CREATE permission on DBDEST
# before using the cloned database, make sure you grant privileges for the user that is set in moodle config

# this database will be read, it's the original version that we need to backup with mysqldump
# it's content should not be changed by the scripts
DBSOURCE="moodlep3jm1"

# this database is the tempory cloned version
# IT WILL DESTROYED (DROP DATABASE) and recreated starting with the data from DBSOURCE
DBTMP="moodlep3cloneTmp"

# this database is the cloned version
# IT WILL DESTROYED (DROP DATABASE) and recreated starting with the data from DBTMP
DBDEST="moodlep3clone"

# Password that will be forced to the moodle admin user
# We dont know what it is, so we have to rest it
PWDEST="ChoseANewMoodleAdminPW!"