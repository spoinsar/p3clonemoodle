-- get those values from a working instance of moodle.


-- `mdl_user_private_key` (`id`, `script`, `value`, `userid`, `instance`, `iprestriction`, `validuntil`, `timecreated`) VALUES (1,'core_files', ***@userkey*** ,4,NULL,NULL,NULL,1607324401);
SET @userkey='replace-this-with-moodle-userkey';

-- `mdl_external_tokens` (`id`, `token`, `privatetoken`, `tokentype`, `userid`, `externalserviceid`, `sid`, `contextid`, `creatorid`, `iprestriction`, `validuntil`, `timecreated`, `lastaccess`) VALUES (1,***@token***,***@privtoken***,0,4,3,NULL,1,2,NULL,0,1607324254,1607324401);
-- first one is token that should be used for webservice scripts
SET @token='replace-this-with-moodle-ws-token';
SET @privtoken='replace-this-with-moodle-ws-private-token';