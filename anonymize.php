<?php
include_once 'db.php';
$db_name = $argv[1];
$hash_key = parse_ini_file('config/hashkey.ini', true)['key'];
$connection_info = parse_ini_file('config/p3.cnf', true)['clientdest'];

if(strlen($hash_key) === 0 || $hash_key === 'replace_by_key'){
    echo 'Vous n\'avez Renseignez de clé dans le fichier suivant config/hashkey.ini';
    return;
}

$dbsdest = new dbdest($connection_info, $db_name);
$users = $dbsdest->get_users();

foreach ($users as $user) {
    if($user->username != 'admin' && $user->username != 'guest'){
        if(!$dbsdest->anonymous_user($user, $hash_key)){
            echo 'Une erreur dans l\'anonymisation des utilisateurs c\'est produite veuillez relancer le script, user :' . $user->id;
            return;
        }

    }
}
echo true;