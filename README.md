# clone moodle for learning analytics

* clone.sh : clone a moodle, and apply scripts (enable webservice and learning analytics). Will be used for production.
* diff.sh : compare the original and the clone database sql dumps. Useful only for development.

moodleclone (database created) must already be installed.
Everything on it will be overwritten (the database of the copy target will be automaticaly dropped).
